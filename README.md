# csvimport wizard

> This is a SaaS based CSV import tool to allow Mac users to import CSV data into OpenAir. Project can be reached at [http://csvimport.aws.af.cm/](http://csvimport.aws.af.cm/)

## Technology
* Python 2.7.* /Django 1.4.*
* Twitter Bootstrap RC3
* jQuery 1.9.1
* LESS

## Hosting
* Hosted on AppFog using AWS

## Contact
* contactme at ryancmorrissey dot com