/*
 * Simple drag'n'drop support for HTML5 File API.
 */

function handleFileSelect(e) {
    e.stopPropagation();
    e.preventDefault();
    var file = e.dataTransfer.files[0];
    $(this).html('File Dropped');
    $(this).removeClass('drag-active');
    console.log(file);
}

function handleDragOver(e) {
    e.stopPropagation();
    e.preventDefault();
    e.dataTransfer.dropEffect = 'copy';
    $(this).addClass('drag-active');
    $(this).html('Drop Here');
}

function handleDragLeave(e) {
    e.stopPropagation();
    e.preventDefault();
    $(this).removeClass('drag-active');
    $(this).html('Drag Here');
}

var dropZone = $('.file-drop-zone')[0];
dropZone.addEventListener('dragover', handleDragOver, false);
dropZone.addEventListener('dragleave', handleDragLeave, false);
dropZone.addEventListener('drop', handleFileSelect, false);
