/**
  * jQuery code begins here...
  */
$(document).ready(function() {

    // set row class using map checkbox
    $('.checkbox-map:checked').each(function() {
        $(this).closest('tr').addClass('info');
    });


    $('.checkbox-map').change(function() {
        // set row class for mapped rows
        $(this).closest('tr').toggleClass('info');

        var oaField = $(this).closest('tr').find('td:nth-child(3) input');
        if ($(this).prop('checked') == true) {
            var fieldName = $(this).closest('tr').find('td:first-of-type input').prop('value');
            // sluggify fieldname as spaces are not allowed in the app
            var fieldNameSlug = fieldName.replace(" ", "_", "gi");
            oaField.prop('value', fieldNameSlug);
        } else {
            oaField.prop('value', '');
        }
    });

    // $('.oa-field').on('keyup keypress blur change', function() {
    //     var oa_field_length = $(this).prop('value').length;
    //     var map_checkbox = $(this).closest('tr').find('.checkbox-map');

    //     if (oa_field_length > 0 && map_checkbox.prop('checked') == false) {
    //         map_checkbox.prop('checked', true);
    //         map_checkbox.closest('tr').addClass('info');
    //     }

    //     else if (oa_field_length == 0 && map_checkbox.prop('checked') == true) {
    //         map_checkbox.prop('checked', false);
    //         map_checkbox.closest('tr').removeClass('info');
    //     }
    // });

    $('.typeahead').on('keyup keypress blur change', function() {
        var oa_field_length = $(this).prop('value').length;
        var map_checkbox = $(this).closest('tr').find('.checkbox-map');

        if (oa_field_length > 0 && map_checkbox.prop('checked') == false) {
            map_checkbox.prop('checked', true);
            map_checkbox.closest('tr').addClass('info');
        }

        else if (oa_field_length == 0 && map_checkbox.prop('checked') == true) {
            map_checkbox.prop('checked', false);
            map_checkbox.closest('tr').removeClass('info');
        }
    });

    // change options button class when options are enabled
    var modalCheckboxesChecked = '.checkbox-custom:checked, .checkbox-date:checked, .checkbox-external:checked, .checkbox-constant:checked';
    $(modalCheckboxesChecked).each(function() {
        var optionsBtn = $(this).closest('.options');
        optionsBtn.addClass('btn-primary-text');
    });


    var modalCheckboxes = '.checkbox-custom, .checkbox-date, .checkbox-external, .checkbox-constant';
    $(modalCheckboxes).change(function() {
        // set btn class when has options
        var optionsBtn = $(this).closest('.modal').closest('td').find('.options');

        if ($(modalCheckboxesChecked).length > 0) {
            optionsBtn.removeClass('btn-info');
            optionsBtn.addClass('btn-warning');
        }

        else {
            optionsBtn.removeClass('btn-warning');
            optionsBtn.addClass('btn-info');
        }
    });

    // disable date selection unless date is checked
    $(".checkbox-date").each(function() {
        var date_format = $(this).closest('table').find('.select-date-format');

        date_format.prop('disabled', true);

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }
    });

    $(".checkbox-date").change(function() {
        var date_format = $(this).closest('table').find('.select-date-format');

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }

        else if ($(this).prop('checked') == false) {
            date_format.prop('value', '');
            date_format.prop('disabled', true);
        }
    });

    // disable field lookup unless date is checked
    $(".checkbox-external").each(function() {
        var date_format = $(this).closest('table').find('.input-external-field');

        date_format.prop('disabled', true);

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }
    });

    $(".checkbox-external").change(function() {
        var date_format = $(this).closest('table').find('.input-external-field');

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }

        else if ($(this).prop('checked') == false) {
            date_format.prop('value', '');
            date_format.prop('disabled', true);
        }
    });

    // disable field lookup unless date is checked
    $(".checkbox-constant").each(function() {
        var date_format = $(this).closest('table').find('.input-constant-field');

        date_format.prop('disabled', true);

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }
    });

    $(".checkbox-constant").change(function() {
        var date_format = $(this).closest('table').find('.input-constant-field');

        if ($(this).prop('checked') == true) {
            date_format.prop('disabled', false);
        }

        else if ($(this).prop('checked') == false) {
            date_format.prop('value', '');
            date_format.prop('disabled', true);
        }
    });

    // options modal
    $('.modal').each(function() {
        $(this).modal({
            keyboard: true,
            show: false
        });
    });

    // SSL warning message
    var is_secure = ('https:' == document.location.protocol);

    var ssl_warning = '' +
        '<li>' +
        '<button type="button" id="ssl_warning_btn" class="btn btn-danger navbar-btn">' +
        '<span class="glyphicon glyphicon-warning-sign"></span>  ' +
        'Please click to switch to the secure version of the site!' +
        '</button>' +
        '</li>' +
        '';

    if (!is_secure) {
        // $('#ssl_warning').append(ssl_warning);
        $('#ssl_warning_btn').addClass('animated pulse');

        $('#ssl_warning_btn').click(function() {
            window.location = 'https://' + document.location.host + document.location.pathname;
        });

        $('#ssl_modal').modal({
            backdrop: 'static',
            keyboard: false,
            show: true
        });
    }

});

// get last 3 characters of string
function getFieldSuffix(str) {
    if (str.length > 3) {
        return str.slice(-3);
    }
    else {
        return false;
    }
}
