import settings

from django.conf.urls.defaults import *
from django.views.generic.simple import redirect_to


# MAIN #
# ---- #
urlpatterns = patterns(
    '',
    url(r'^$',
        redirect_to,
        {'url': '/home/'}
        ),
    url(r'^static/(?P<path>.*)$',
        'django.views.static.serve',
        {'document_root': settings.STATIC_ROOT}
        ),
)

# HOMEPAGE #
# -------- #
urlpatterns += patterns(
    'wizard.views',
    url(r'^home/$',
        'home',
        name='home'
        ),
)

# HELP #
# ---- #
urlpatterns += patterns(
    'wizard.views',
    url(r'^help/$',
        'faq',
        name='help'
        ),
    url(r'^faq/$',
        'faq',
        name='faq',
        ),
)

# ADMINISTRATION #
# -------------- #
urlpatterns += patterns(
    'wizard.views',
    url(r'^administration/$',
        'administration',
        name='administration'
        ),
    url(r'^administration/delete/(?P<uuid>[A-Za-z0-9\-]+)/$',
        'delete_file',
        name='delete-file'
        ),
    url(r'^administration/delete_everything/$',
        'delete_everything',
        name='delete-everything'
        ),
)

# WIZARD #
# ------ #
urlpatterns += patterns(
    'wizard.views',
    url(r'^wizard/$',
        'wizard',
        name='wizard'
        ),
    url(r'^wizard/error_code/$',
        'wizard_error_code',
        name='error_code'
        ),
)

# WIZARD BETA #
# ----------- #
urlpatterns += patterns(
    'wizard.beta.views',
    url(r'^wizard/beta/upload/$',
        'wizard_beta_upload',
        name='wizard-beta-upload'
        ),
    url(r'^wizard/beta/mapping/(?P<uuid>[A-Za-z0-9\-]+)/$',
        'wizard_beta_mapping',
        name='wizard-beta-mapping'
        ),
)

# AJAX URLS #
# --------- #
urlpatterns += patterns(
    'wizard.ajax',
    url(r'^wizard/ajax/recordtype/(?P<recordtype>[A-Za-z0-9_]+)/$',
        'get_apimethods',
        name='get-apimethods'
        ),
)

# CUSTOM ERRORS #
# ------------- #
handler403 = 'wizard.views.custom403'
handler404 = 'wizard.views.custom404'
handler500 = 'wizard.views.custom500'

# DEVELOPMENT #
# ----------- #
if settings.DEBUG:
    urlpatterns += patterns(
        'wizard.views',
        url(r'^403/$',
            'custom403',
            name='custom403'
            ),
        url(r'^404/$',
            'custom404',
            name='custom404'
            ),
        url(r'^500/$',
            'custom500',
            name='custom500'
            ),
    )
