from __future__ import absolute_import

from django.http import HttpResponse

from wizard import apimethods

try:
    import simplejson as json
except ImportError:
    import json


# AJAX REQUESTS #
# ------------- #
def get_apimethods(request, recordtype=None):
    """
    Ajax request from upload form.

    """
    if recordtype:
        options = apimethods.API_METHODS[recordtype]
        print options
    else:
        options = []

    json_options = json.dumps(options)
    return HttpResponse(json_options, mimetype='application/json')
