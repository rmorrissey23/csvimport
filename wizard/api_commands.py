from __future__ import absolute_import

import csv
import io
import urllib2

from xml.sax.saxutils import escape
from credentials import OPENAIR_API

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


class Status:
    """
    This class allows status messages to be more easily
    transported between functions and views.
    """
    def __init__(self, status, data):
        self.status = status
        self.data = data


class DictReaderInsensitive(csv.DictReader):
    """
    This class overrides the csv.fieldnames property.
    All fieldnames are without whitespace and in lowercase.
    """
    @property
    def fieldnames(self):
        if self._fieldnames is None:
            try:
                self._fieldnames = next(self.reader)
            except StopIteration:
                pass

        self.line_num = self.reader.line_num
        if not isinstance(self._fieldnames, ListInsensitive):
            self._fieldnames = ListInsensitive(
                name.strip().lower() for name in self._fieldnames
            )
        return self._fieldnames

    def __next__(self):
        """
        Override the original __next__ method and store in
        DictReaderInsensitive instead.
        """
        dInsensitive = DictInsensitive()
        dOriginal = super(DictReaderInsensitive, self).__next__()

        for key, value in dOriginal.items():
            dInsensitive[key] = value

        return dInsensitive


class ListInsensitive(list):
    """
    Override the standard list with a case insensitive list.
    """
    def __contains__(self, item):
        return list.__contains__(self, item.strip().lower())


class DictInsensitive(dict):
    """
    This class overrides the __getitem__ method to automatically
    strip() and lower() the input key.
    """
    def __getitem__(self, key):
        return dict.__getitem__(self, key.strip().lower())


xml_escape_table = {
    '"': '&quot;',
    "'": '&apos;'
}


def xml_escape(string):
    """
    Escape the 5 characters needed for XML processing.
    """
    return escape(string, xml_escape_table)


def process_csvfile_from_database(csvfile):
    """
    Process csv file for data.

    Args:
       csvfile (obj): A Python file object.

    Returns:
       dict: status and data (csv data list or error)

    """
    try:
        f = io.StringIO(
            unicode(csvfile.file.read(), 'ISO-8859-1', errors='ignore'),  # utf-8 didnt work
            newline=None
        )

        try:
            r = DictReaderInsensitive(
                f,
                dialect='excel',
                delimiter=b','
            )

            headers = r.fieldnames

            return Status(
                0,
                {"headers": headers, "reader": r}
            )

        except:
            return Status(
                1,
                "Error creating dictreader."
            )

    except:
        return Status(
            1,
            "Unable to process CSV file."
        )


def process_csv_file(csvfile):
    """
    Process csv file for data.

    Args:
       csvfile (obj): A valid csv file object.

    Returns:
       dict: status and data (csv data list or error)

    """
    try:
        f = io.StringIO(
            unicode(csvfile.read(), 'ISO-8859-1', errors='ignore'),  # utf-8 didnt work
            newline=None
        )

        try:
            r = DictReaderInsensitive(
                f,
                dialect='excel',
                delimiter=b','
            )

            headers = r.fieldnames

            return Status(
                0,
                {"reader": r, "headers": headers}
            )

        except:
            return Status(
                1,
                "Error creating dictreader."
            )

    except:
        return Status(
            1,
            "Unable to process CSV file."
        )


def create_xml_login(data, company, user, password, server):
    """
    Provide a XML based login to the OpenAir API.

    Args:
        data (str): XML formatted data to be sent via the HTTP post.
        company (str): The company ID for login.
        user (str): The user ID for login.
        password (str): The password for login.
        server (str): The server where the account is located.

    Returns:
        dict: status and data (client object or error)

    """
    try:
        xml_auth = (
            '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
            '<request API_version="1.0" client="%s" client_ver="1.0" '
            'namespace="%s" key="%s"><Auth><Login><company>%s</company>'
            '<user>%s</user><password>%s</password></Login></Auth>'
        ) % (OPENAIR_API['client'],
             OPENAIR_API['namespace'],
             OPENAIR_API['keys'][server],
             xml_escape(company),
             xml_escape(user),
             xml_escape(password)
             )
        xml_auth += data + '</request>'

        return Status(
            0,
            xml_auth
        )

    except:
        return Status(
            0,
            "Unable to form login request."
        )


def generate_xml_record_data(recordtype, headers, dictreader):
    """
    Creates a string of XML tags to be sent to the API. Tags will
    contain data pulled from the CSV file rows. Use this to create
    a string to be passed to the auth request command.

    recordtype: valid recordtype object
    fieldnames: list of header fieldnames
    dictreader: dictreader object from csv module
    """
    try:
        if recordtype and headers and dictreader:
            xml_data = ''

            try:
                for row in dictreader:
                    xml_data += '<Modify type="%s"><%s>' % (
                        recordtype.capitalize(),
                        recordtype.capitalize()
                    )

                    try:
                        for field in OPENAIR_API['fields'][recordtype]:
                            if field.lower() in OPENAIR_API['fields']['address']:
                                xml_data += '<addr><Address><%s>%s</%s>' % (
                                    field.lower(),
                                    xml_escape(row[field]),
                                    field.lower()
                                )
                                xml_data += '</Address></addr>'
                            else:
                                xml_data += '<%s>%s</%s>' % (
                                    field.lower(),
                                    xml_escape(row[field]),
                                    field.lower()
                                )

                        xml_data += '</%s></Modify>' % recordtype.capitalize()

                    except:
                        return Status(
                            1,
                            ("Error processing fields in row. "
                             "Check that you don\'t have invalid characters.")
                        )

                return Status(0, xml_data.encode('utf-8'))

            except:
                return Status(
                    1,
                    ("Error processing row. "
                     "Check that you don\'t have invalid characters.")
                )

        else:
            return Status(1, "Missing required parameters.")

    except:
        return Status(1, "Error processing dictreader.")


def generate_xml_switch_data(headers, dictreader):
    """
    Creates a string of XML tags to be sent to the API. Tags will
    contain data pulled from the CSV file rows. Use this to create
    a string to be passed to the auth request command.

    headers: list of header headers
    dictreader: dictreader object from csv module
    """
    try:
        if headers and dictreader:
            xml_data = ''

            for row in dictreader:
                for field in OPENAIR_API['fields']['switch']:
                    xml_data += (
                        '<Modify type="Company"><Company><id>%s</id><flags>'
                        '<Flag><name>%s</name><setting>%s</setting></Flag>'
                        '</flags></Company></Modify>'
                    ) % (
                        field.lower(),
                        xml_escape(row[field]),
                        field.lower()
                    )

            return Status(0, xml_data.encode('utf-8'))

        else:
            return Status(1, "Missing required parameters!")

    except:
        return Status(1, "Error processing dictreader.")


def generate_servertime_data():
    """
    Return the servertime using the OpenAir API.

    Args:
        none

    Returns:
        dict: status and data (string)

    """
    try:
        servertime = '<Time></Time>'

        return Status(0, servertime)

    except:
        return Status(1, "Error retrieving servertime.")


def generate_api_request(login_data, server):
    """
    Return the servertime using the OpenAir API.

    Args:
        login_data(str): xml string for passing to the API.
        server(str): a valid server url.

    Returns:
        str: xml response

    """
    req = urllib2.Request(
        url='https://%s/api.pl' % server,
        data=login_data
    )
    res = urllib2.urlopen(req)
    xml_res = res.read()

    return xml_reader(xml_res)


def return_error_code(code):
    """
    Returns a detailed message about the error code.

    Args:
        code(str): A 3-digit error code.

    Returns:
        str: an error message
    """
    xml_data = (
        '<?xml version="1.0" encoding="UTF-8" standalone="yes"?>'
        '<request API_version="1.0" client="%s" client_ver="1.0" '
        'namespace="%s" key="%s"><Read type="Error" method="equal to">'
        '<Error><code>%s</code></Error></Read></request>'
    ) % (
        OPENAIR_API['client'],
        OPENAIR_API['namespace'],
        OPENAIR_API['keys']['qa.openair1.com'],
        code
    )

    req = urllib2.Request(
        url='https://qa.openair1.com/api.pl',
        data=xml_data
    )
    res = urllib2.urlopen(req)
    xml_res = res.read()

    # return return_xml_status(xml_res)
    return xml_reader(xml_res)


def xml_reader(xml_string):
    """
    Something here...
    """
    try:
        root = ET.fromstring(xml_string)

        # Check if authentication was used
        if root.findall('Auth'):

            # Proceed if authentication is successful
            if root.find('Auth').attrib['status'] == '0':

                # Check if servertime was used
                if root.findall('Time'):

                    # Proceed if servertime is successful
                    if root.find('Time').attrib['status'] == '0':

                        m = root.findtext('Time/Date/month')
                        d = root.findtext('Time/Date/day')
                        y = root.findtext('Time/Date/year')
                        H = root.findtext('Time/Date/hour')
                        M = root.findtext('Time/Date/minute')
                        S = root.findtext('Time/Date/second')

                        if not H:
                            H = '12'
                        if not M:
                            M = '00'
                        if not S:
                            S == '00'

                        servertime = '%s/%s/%s %s:%s:%s EDT' % (
                            m, d, y, H, M, S
                        )

                        return Status(0, servertime)

                    # Servertime unsuccessful
                    else:
                        return Status(1, "Unable to return servertime.")

                elif root.findall('Modify'):

                    modify_elems = root.findall('Modify')
                    msg = ''

                    for num, elem in enumerate(modify_elems, start=2):

                        # set status row color
                        if elem.attrib['status'] == '0':
                            klass = 'success'
                            code = 'Modify successful'
                        else:
                            klass = 'danger'
                            code = 'Error code: %s' % (
                                elem.attrib['status'],
                            )

                        msg += '<tr class="%s"><td>%d</td><td>%s</td></tr>' % (
                            klass,
                            num,
                            code
                        )

                    return Status(0, msg)

                else:
                    return Status(0, "Authentication successful.")

            # Authentication failed
            else:
                return Status(
                    1,
                    "Authentication error (%s)" % (
                        root.find('Auth').attrib['status'],
                    )
                )

        # Check if error was used
        elif root.findall('Read/Error'):

            # Proceed if the error read was successful
            if root.find('Read').attrib['status'] == '0':

                text = root.findtext('Read/Error/text').capitalize()
                comment = root.findtext('Read/Error/comment').capitalize()

                return Status(
                    0,
                    '<span class="text-error">%s.</span> %s' % (
                        text,
                        comment
                    )
                )

            # Error read was unsuccessful
            else:
                return Status(1, "Error read was unsuccessful.")

        elif root.tag == 'response':
            text = root.text.capitalize()

            return Status(
                0,
                '<span class="text-error">%s.</span>' % (
                    text
                )
            )

        # Empty XML response - return as error for now
        else:
            return Status(1, "Empty XML response.")

    # Parsing error
    except:
        return Status(1, "Error parsing XML string.")


def xml_res_parser(xml_string, recordtype, apimethod):
    """
    Something here...
    """
    root = ET.fromstring(xml_string)
    elems = root.findall(apimethod)
    results = []

    for num, elem in enumerate(elems, start=2):
        if elem.attrib['status'] == '0':
            record_id = elem.findtext(recordtype + '/id')
            klass = 'success'
            kode = '%s status: Success for id %s' % (apimethod, record_id)
        else:
            klass = 'danger'
            kode = 'Error code: %s' % elem.attrib['status']

        results.append({'klass': klass, 'kode': kode})

    return results


def xml_res_auth(xml_string):
    """
    Something here...
    """
    root = ET.fromstring(xml_string)
    status = root.find('Auth').attrib['status']
    results = None

    if status == '0':
        results = 'Success'
    else:
        results = 'Error code: %s' % status

    return results
