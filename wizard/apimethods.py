"""
This file holds a supported list of complex record types
as well as the associated API methods that each record
type supports.

New records should be added in a specific format:

EX// 'Recordtype': [{'method': 'Method'}]

"""

API_METHODS = {
    'Budget': [
        {'method': 'Add'},
        {'method': 'Modify'}
    ],
    'Customer': [
        {'method': 'Add'},
        {'method': 'Modify'}
    ],
    'Project': [
        {'method': 'Add'},
        {'method': 'Modify'}
    ],
    'Projecttask': [
        {'method': 'Add'},
        {'method': 'Modify'}
    ],
    'Slip': [
        {'method': 'Add'},
        {'method': 'Modify'}
    ],
    'Task': [
        {'method': 'Modify'}
    ],
    'Ticket': [
        {'method': 'Modify'}
    ],
    'User': [
        {'method': 'Modify'}
    ]
}
