from __future__ import absolute_import

from os import path

try:
    import simplejson as json
except ImportError:
    import json

from django import forms
from django.core.exceptions import ValidationError

from credentials import OPENAIR_API


# VARIABLES #
# --------- #
SERVER_LIST = (
    ('', ''),
    ('demo.openair.com', 'demo.openair.com'),
    ('qa.openair1.com', 'qa.openair1.com'),
    ('sandbox.openair.com', 'sandbox.openair.com'),
)

RECORDTYPES = (
    ('', ''),
    ('Slip', 'Charge (slip)'),
    ('Customer', 'Customer (customer)'),
    ('Project', 'Project (project)'),
    ('Budget', 'Project budget (budget)'),
    ('Projecttask', 'Project task (projecttask)'),
    ('Ticket', 'Receipt (ticket)'),
    ('Task', 'Time entry (task)'),
    ('User', 'User (user)'),
)

API_METHODS = (
    ('', ''),
    ('Add', 'Add'),
    ('Modify', 'Modify'),
)

DATEFORMATS = (
    ('', ''),
    ('%m/%d/%Y', 'MM/DD/YYYY'),
    ('%m/%d/%y', 'MM/DD/YY'),
    ('%d/%m/%Y', 'DD/MM/YYYY'),
    ('%d/%m/%y', 'DD/MM/YY'),
    ('%m-%d-%Y', 'MM-DD-YYYY'),
    ('%m-%d-%y', 'MM-DD-YY'),
    ('%d-%m-%Y', 'DD-MM-YYYY'),
    ('%d-%m-%y', 'DD-MM-YY'),
    ('%Y-%m-%d', 'YYYY-MM-DD'),
    ('%m%d%Y', 'MMDDYYYY'),
)


# FIELDS #
# ------ #
class CsvFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        super(CsvFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(CsvFileField, self).clean(*args, **kwargs)
        if data:
            head, tail = path.splitext(data.name)
            tail = tail.lower()

            if not tail == '.csv':
                err = self.error_messages['invalid']
                raise ValidationError(err)

            return data


class SecretKeyField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super(SecretKeyField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        secretkey = super(SecretKeyField, self).clean(*args, **kwargs)
        if not secretkey == OPENAIR_API['secretkey']:
            err = self.error_messages['invalid']
            raise ValidationError(err)

        return secretkey


# WIZARD FORM BETA UPLOAD #
# ----------------------- #
class WizardFormBetaUpload(forms.Form):
    """
    A form for importing csv files.
    """
    recordtype = forms.ChoiceField(
        required=True,
        error_messages={
            'required': 'Please select a record type!'
        },
        initial='',
        choices=RECORDTYPES,
        widget=forms.Select(
            attrs={
                'id': 'recordtype',
                'class': 'form-control input-lg',
                'data-placeholder': 'type to search record types...'
            }
        )
    )
    apimethod = forms.ChoiceField(
        required=True,
        error_messages={
            'required': 'Please select an import method!'
        },
        initial='',
        choices=API_METHODS,
        widget=forms.Select(
            attrs={
                'id': 'apimethod',
                'class': 'form-control input-lg',
                'data-placeholder': 'type to search allowed methods...'
            }
        )
    )
    csvfile = CsvFileField(
        required=True,
        error_messages={
            'invalid': 'A file ending in .csv is required!'
        },
        widget=forms.FileInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'csvfile'
            }
        )
    )


# WIZARD FORM BETA LOGIN #
# ---------------------- #
class WizardFormBetaLogin(forms.Form):
    """
    A form for capturing login information.
    """
    company = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A company name is required!'
        },
        widget=forms.TextInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'login_company'
            }
        )
    )
    user = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A username is required!'
        },
        widget=forms.TextInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'login_user'
            }
        )
    )
    password = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A password is required!'
        },
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'login_password'
            }
        )
    )
    server = forms.ChoiceField(
        required=True,
        error_messages={
            'required': 'A server is required!'
        },
        initial='demo.openair.com',
        choices=SERVER_LIST,
        widget=forms.Select(
            attrs={
                'id': 'login_server',
                'class': 'form-control input-lg',
                'data-placeholder': 'type to select a server URL...'
            }
        )
    )
    secretkey = SecretKeyField(
        max_length=5,
        required=True,
        error_messages={
            'required': 'A secret key is required!',
            'invalid': 'You have provided an invalid secret key!'
        },
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'login_secretkey',
                'placeholder': 'a secret key is required!'
            }
        )
    )
    detailed_log = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'id': 'login_detailed_log',
                'checked': 'checked'
            }
        )
    )


# WIZARD FORM BETA MAPPING #
# ------------------------ #
class WizardFormBetaMapping(forms.Form):
    """
    A form for mapping CSV header names to OA database fields

    """
    def __init__(self, *args, **kwargs):
        recordtype = kwargs.pop('recordtype', None)
        super(WizardFormBetaMapping, self).__init__(*args, **kwargs)
        if recordtype is not None:
            self.recordtype = recordtype

    file_field = forms.CharField(
        max_length=200,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control text-primary',
                'readonly': 'readonly',
                'tabindex': '-1'
            }
        )
    )
    map_fields = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'form-control checkbox-map'
            }
        )
    )
    oa_field = forms.CharField(
        max_length=200,
        required=False,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control typeahead',
                'placeholder': 'type to add field...'
            }
        )
    )
    date_field = forms.BooleanField(
        required=False,
        widget=forms.CheckboxInput(
            attrs={
                'class': 'form-control checkbox-date'
            }
        )
    )
    date_format = forms.ChoiceField(
        required=False,
        initial='',
        choices=DATEFORMATS,
        widget=forms.Select(
            attrs={
                'class': 'form-control select-date-format',
                'disabled': 'disabled'
            }
        )
    )

    def clean(self):
        """
        Custom clean method...

        """
        cleaned_data = self.cleaned_data
        map_fields = cleaned_data['map_fields']
        oa_field = cleaned_data['oa_field']
        date_field = cleaned_data['date_field']
        date_format = cleaned_data['date_format']

        if date_field and not date_format:
            # check for date_format when date_field
            err = 'You must specify a matching date format in "Options"!'
            self._errors['date_format'] = self.error_class([err])
            # remove field from cleaned_data
            del cleaned_data['date_format']

        if map_fields and not oa_field:
            # require an OA field when fields are marked as mapped
            err = 'You must provide a NetSuite OpenAir field when mapping!'
            self._errors['oa_field'] = self.error_class([err])
            # remove field from cleaned_data
            del cleaned_data['oa_field']

        return self.cleaned_data
