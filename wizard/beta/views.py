from __future__ import absolute_import

from django.contrib import messages
# from django.core.exceptions import ValidationError
from django.core.files.storage import default_storage
from django.core.urlresolvers import reverse
from django.forms.formsets import BaseFormSet, formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.debug import sensitive_variables

from wizard.beta.forms import WizardFormBetaUpload, WizardFormBetaLogin, WizardFormBetaMapping

from wizard.models import CSVFile

from credentials import OPENAIR_API
import wizard.api_commands as api_commands
import wizard.oaxmlapi as oaxmlapi

import datetime
from functools import partial, wraps
# from os import path
import re
import urllib2
import xml.dom.minidom as xmlmini

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

try:
    import simplejson as json
except ImportError:
    import json


# HELPER #
# ------ #
def clean_xml_values(string):
    """
    Removes any passwords and secret keys from an XML string.

    """
    safe_xml_string = re.sub(
        r'<password>.*?<\/password>',
        '<password>*****</password>',
        string,
        flags=re.S
    )
    safe_xml_string = re.sub(
        r'\skey="([A-Za-z0-9]*?)"',
        ' key="*****"',
        safe_xml_string,
        count=1
    )
    return safe_xml_string


def get_complex_type_fields(recordtype):
    """
    Opens and processes a json file using the recordtype as the key.

    Returns:
        {@list}: a jsonified list object

    """
    with open('wizard/openair_wsdl.json') as f:
        json_data = json.load(f)
        fields = json_data[recordtype]
        choices_list = []
        for field in fields:
            choices_list.append(field)
    return json.dumps(choices_list)


# WIZARD BETA #
# ----------- #
def wizard_beta_upload(request):
    """
    A form for uploading CSV files and tagging that file with a recordtype and API method.
    These tags are used later to support field mapping.

    Arguments:
        request {@object}: a Django request object

    Returns:
        {@object}: a Django render_to_response object

    """
    if request.POST and request.method == 'POST':
        form = WizardFormBetaUpload(request.POST, request.FILES)

        if form.is_valid():
            c_recordtype = form.cleaned_data['recordtype']
            c_apimethod = form.cleaned_data['apimethod']

            # store a record of this uploaded file in a database table with related tag fields
            csvfile = CSVFile(
                file=request.FILES['csvfile'],
                recordtype=c_recordtype,
                apimethod=c_apimethod
            )

            csvfile.save()

            # this redirect points to the get_absolute_url method of the CSVFile object
            return redirect(csvfile)

        else:
            template = 'beta/wizard_upload.html'
            args = {
                'tab': 'wizard-beta',
                'form': form
            }

            return render_to_response(
                template,
                args,
                context_instance=RequestContext(request)
            )
    else:
        form = WizardFormBetaUpload()

    template = 'beta/wizard_upload.html'
    args = {
        'tab': 'wizard-beta',
        'form': form
    }

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request)
    )


@sensitive_variables('c_password', 'xml_login', 'uuid')
def wizard_beta_mapping(request, uuid=None):
    """
    A mapping form which is composed of two separate form objects: a basic form for gathering
    account login information and a secretkey for validation, and a formset for gathering field
    mapping information from the user to help process the CSV file records.

    Arguments:
        request {@object}: a Django request object
        uuid {@string}: a uuid string for a CSV file stored in the database

    """
    if uuid:
        # look for the CSVFile object by uuid rather than pkey since we get the uuid from the URL
        csvfile = get_object_or_404(CSVFile, uuid__exact=uuid)

        f = api_commands.process_csvfile_from_database(csvfile.file)
        recordtype = csvfile.recordtype.capitalize()
        apimethod = csvfile.apimethod.capitalize()

        if f.status == 0:
            headers = f.data['headers']
            dictreader = f.data['reader']

        else:
            headers = []
            dictreader = []

    # set initial data for form - should just be field headers from file
    initial_data = []
    for field in headers:
        initial_data.append({u'file_field': u'%s' % field})

    WizardFormSetBetaMapping = formset_factory(
        # wrap the form with a keyword argument using functools
        wraps(WizardFormBetaMapping)(
            partial(
                WizardFormBetaMapping,
                recordtype=recordtype
            )
        ),
        formset=BaseFormSet,
        extra=0
    )

    if request.POST and request.method == 'POST':
        # form for login credentials
        form = WizardFormBetaLogin(request.POST)
        # formset for mapping each field
        formset = WizardFormSetBetaMapping(request.POST)

        if form.is_valid():
            c_company = form.cleaned_data['company']
            c_user = form.cleaned_data['user']
            c_password = form.cleaned_data['password']
            c_server = form.cleaned_data['server']
            c_detailed_log = form.cleaned_data['detailed_log']

            if formset.is_valid():
                field_collection = []
                results = []

                for form in formset:
                    c_file_field = form.cleaned_data['file_field']
                    c_map_fields = form.cleaned_data['map_fields']
                    c_oa_field = form.cleaned_data['oa_field']
                    c_date_field = form.cleaned_data['date_field']
                    c_date_format = form.cleaned_data['date_format']

                    # only proceed for mapped fields
                    mapping_details = {}
                    if c_map_fields:
                        # just used for debugging and testing right now...
                        mappings = {}
                        fields = {}
                        options = {}
                        mappings['fields'] = fields
                        mappings['options'] = options

                        mapped_fields, extras = {}, {}
                        mapping_details['mapped_fields'] = mapped_fields
                        mapping_details['extras'] = extras

                        # get field values here
                        mappings['fields']['file_field'] = c_file_field
                        mappings['fields']['oa_field'] = c_oa_field
                        mapping_details['mapped_fields'][c_oa_field] = c_file_field

                        if c_date_field:
                            mappings['options']['date_field'] = c_date_format
                            mapping_details['extras']['date_field'] = c_date_format

                        results.append(mappings)
                        field_collection.append(mapping_details)

                enable_custom = {'enable_custom': '1'}

                mapping_data = []
                totalrows = 0

                for i, row in enumerate(dictreader):

                    # TODO: This is a lazy way to support the API limit of 1000 objects.
                    #       A more appropriate method would be to configure the tool
                    #       to loop through the dictreader and queue up 1000 object chunks
                    #       that would be processed in order. This might require the use
                    #       of Python threading (research needed).

                    if i >= 1000:
                        print "DEBUG>>> Total CSV rows processed =", totalrows
                        break

                    else:
                        fields_dict = {}

                        # iterate through only those fields which were mapped
                        for mapping in field_collection:
                            for field in mapping['mapped_fields'].keys():
                                field_value = row[mapping['mapped_fields'][field]]

                                # if the mapped field is marked as a date field,
                                # treat it as special
                                if mapping['extras'] and 'date_field' in mapping['extras'].keys():
                                    date_format = mapping['extras']['date_field']

                                    try:
                                        # try to convert date string to date object
                                        date = datetime.datetime.strptime(
                                                                field_value,
                                                                date_format
                                                                ).date()

                                    except ValueError as e:
                                        # if conversion fails, report the error and exit
                                        messages.error(
                                            request,
                                            str(e),
                                            extra_tags='error'
                                        )
                                        return HttpResponseRedirect(
                                            reverse(
                                                'wizard-beta-mapping',
                                                kwargs={'uuid': uuid}
                                            )
                                        )

                                    # if the destination date field is a custom date field,
                                    # convert date to SQL string as expected by SOAP; else,
                                    # create an oaDate complex type object
                                    if field[-3:] == '__c':
                                        # field is custom
                                        d_date = '{0}-{1}-{2}'.format(
                                                                str(date.year),
                                                                str(date.month),
                                                                str(date.day)
                                                                )
                                    else:
                                        d_date = oaxmlapi.Datatype(
                                            'Date',
                                            {
                                                'month': str(date.month),
                                                'day': str(date.day),
                                                'year': str(date.year)
                                            }
                                        )

                                    fields_dict[field] = d_date
                                else:
                                    fields_dict[field] = api_commands.xml_escape(field_value)

                        d_datatype = oaxmlapi.Datatype(recordtype, fields_dict)

                        # create datatype for add/update
                        if apimethod == 'Add':
                            if recordtype == 'User':
                                d_company = oaxmlapi.Datatype(
                                    'Company',
                                    {
                                        "nickname": api_commands.xml_escape(c_company)
                                    }
                                )
                                mapping_data.append(
                                    oaxmlapi.CreateUser(
                                        d_company,
                                        d_datatype
                                    ).create()
                                )
                            else:
                                # do an add
                                mapping_data.append(
                                    oaxmlapi.Add(
                                        recordtype,
                                        enable_custom,
                                        d_datatype
                                    ).add()
                                )

                        elif apimethod == 'Modify':
                            # do a modify
                            mapping_data.append(
                                oaxmlapi.Modify(
                                    recordtype,
                                    enable_custom,
                                    d_datatype
                                ).modify()
                            )

                        totalrows += 1

                app = oaxmlapi.Application(
                    OPENAIR_API['client'],
                    OPENAIR_API['version'],
                    OPENAIR_API['namespace'],
                    OPENAIR_API['keys'][c_server]
                )

                auth = oaxmlapi.Auth(
                    c_company,
                    c_user,
                    c_password,
                )

                xml_req = oaxmlapi.Request(
                    app,
                    auth,
                    mapping_data[0:1000]
                ).tostring()

                req = urllib2.Request(
                    url='https://{0}/api.pl'.format(c_server),
                    data=xml_req.encode('utf-8')
                )

                res = urllib2.urlopen(req)
                xml_res = res.read()

                auth_status = api_commands.xml_res_auth(xml_res)
                res_status = api_commands.xml_res_parser(xml_res, recordtype, apimethod)

                pp_safe_req, pp_safe_res = None, None

                if c_detailed_log:
                    safe_xml_req = clean_xml_values(xml_req)
                    safe_xml_res = clean_xml_values(xml_res)

                    safe_req = xmlmini.parseString(safe_xml_req)
                    pp_safe_req = safe_req.toprettyxml(indent="    ")

                    safe_res = xmlmini.parseString(safe_xml_res)
                    pp_safe_res = safe_res.toprettyxml(indent="    ")

                template = 'beta/wizard_results.html'

                args = {
                    'tab': 'wizard-beta',
                    'csvfile': csvfile,
                    'totalrows': totalrows,
                    'auth_status': auth_status,
                    'res_status': res_status,
                    'detailed_log': c_detailed_log,
                    'pp_safe_req': pp_safe_req,
                    'pp_safe_res': pp_safe_res
                }

                return render_to_response(
                    template,
                    args,
                    context_instance=RequestContext(request)
                )

    else:
        form = WizardFormBetaLogin()
        formset = WizardFormSetBetaMapping(initial=initial_data)

    template = 'beta/wizard_mapping.html'
    args = {
        'tab': 'wizard-beta',
        'csvfile': csvfile,
        'form': form,
        'formset': formset,
        'js_field_list': get_complex_type_fields(recordtype)
    }

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request)
    )
