from __future__ import absolute_import

from os import path

try:
    import simplejson as json
except ImportError:
    import json

from django import forms
from django.core.exceptions import ValidationError

from credentials import OPENAIR_API


# VARIABLES #
# --------- #
SERVER_LIST = (
    ('', ''),
    ('demo.openair.com', 'demo.openair.com'),
    ('qa.openair1.com', 'qa.openair1.com'),
    ('sandbox.openair.com', 'sandbox.openair.com'),
)

RECORDTYPES = (
    ('', ''),
    ('customer', 'Customer'),
    ('project', 'Project'),
    ('user', 'Employee'),
    ('servertime', '~Servertime~'),
)


# FIELDS #
# ------ #
class CsvFileField(forms.FileField):
    def __init__(self, *args, **kwargs):
        super(CsvFileField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        data = super(CsvFileField, self).clean(*args, **kwargs)
        if data:
            head, tail = path.splitext(data.name)
            tail = tail.lower()

            if not tail == '.csv':
                err = self.error_messages['invalid']
                raise ValidationError(err)

            return data


class SecretKeyField(forms.CharField):
    def __init__(self, *args, **kwargs):
        super(SecretKeyField, self).__init__(*args, **kwargs)

    def clean(self, *args, **kwargs):
        secretkey = super(SecretKeyField, self).clean(*args, **kwargs)
        if not secretkey == OPENAIR_API['secretkey']:
            err = self.error_messages['invalid']
            raise ValidationError(err)

        return secretkey


# WIZARD FORM #
# ----------- #
class WizardForm(forms.Form):
    """
    A form for importing csv files.
    """
    recordtype = forms.ChoiceField(
        required=True,
        error_messages={
            'required': 'Please select a record type!'
        },
        initial='',
        choices=RECORDTYPES,
        widget=forms.Select(
            attrs={
                'id': 'recordtype',
                'class': 'form-control input-lg'
            }
        )
    )
    csvfile = CsvFileField(
        required=False,
        error_messages={
            'invalid': 'A file ending in .csv is required!'
        },
        widget=forms.FileInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'csvfile'
            }
        )
    )
    company = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A company name is required!'
        },
        widget=forms.TextInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'company'
            }
        )
    )
    user = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A username is required!'
        },
        widget=forms.TextInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'user'
            }
        )
    )
    password = forms.CharField(
        max_length=200,
        required=True,
        error_messages={
            'required': 'A password is required!'
        },
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'password'
            }
        )
    )
    server = forms.ChoiceField(
        required=True,
        error_messages={
            'required': 'A server is required!'
        },
        initial='demo.openair.com',
        choices=SERVER_LIST,
        widget=forms.Select(
            attrs={
                'id': 'server',
                'class': 'form-control input-lg'
            }
        )
    )
    secretkey = SecretKeyField(
        max_length=5,
        required=True,
        error_messages={
            'required': 'A secret key is required!',
            'invalid': 'You have provided an invalid secret key!'
        },
        widget=forms.PasswordInput(
            attrs={
                'class': 'form-control input-lg',
                'id': 'secretkey'
            }
        )
    )
    confirm = forms.BooleanField(
        required=True,
        error_messages={
            'required': 'You must check this box to continue!'
        },
        widget=forms.CheckboxInput(attrs={
            'id': 'confirm'}
        )
    )

    def clean(self):
        """
        Only require a file if a valid recordtype
        is selected. Skip for test methods.
        """
        cleaned_data = self.cleaned_data
        recordtype = cleaned_data.get('recordtype')
        file_required = ('customer', 'project', 'user',)
        if recordtype in file_required:
            # conditionally require csvfile field
            if cleaned_data.get('csvfile') is None:
                err = 'A file ending in .csv is required!'
                self._errors['csvfile'] = self.error_class([err])

        return cleaned_data


# ERROR LOOKUP FORM #
# ----------------- #
class ErrorCodeForm(forms.Form):
    """
    A form for importing csv files.
    """
    error_code = forms.CharField(
        max_length=4,
        required=True,
        widget=forms.TextInput(
            attrs={
                'class': 'form-control',
                'id': 'error_code',
                'placeholder': 'Error code',
                'autofocus': 'autofocus'
            }
        )
    )
