from os import path

from django.db import models

from django_extensions.db import fields


class CSVFile(models.Model):
    """
    A file object.
    """
    uuid = fields.UUIDField(max_length=36, unique=True)
    file = models.FileField(max_length=100, upload_to='beta')
    recordtype = models.CharField(max_length=100)
    apimethod = models.CharField(max_length=10)
    created = models.DateTimeField(auto_now_add=True)

    def __unicode__(self):
        return u'(%s) %s' % (self.recordtype, self.uuid)

    @property
    def get_filename(self):
        """
        Returns the file name without the full path

        """
        return "%s" % path.basename(self.file.url)

    @models.permalink
    def get_absolute_url(self):
        return ('wizard.beta.views.wizard_beta_mapping', [str(self.uuid)])

    @models.permalink
    def get_delete_url(self):
        return ('wizard.views.delete_file', [str(self.uuid)])

    class Meta:
        db_table = 'csvfile'
        ordering = ['-id']
