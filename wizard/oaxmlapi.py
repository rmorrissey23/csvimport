from __future__ import absolute_import
from datetime import datetime
from xml.dom import minidom

try:
    import simplejson as json
except ImportError:
    import json

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET


# VARIABLES #
# --------- #
ADDRESS_FIELDS = (
    'first', 'middle', 'last', 'salutation', 'email', 'phone',
    'fax', 'mobile', 'addr1', 'addr2', 'addr3', 'addr4',
    'city', 'state', 'zip', 'country'
)


# UTILITY METHODS #
# --------------- #
def elem2dict(elem, strip=True):
    """
    Convert an ElementTree() object into a Python dictionary.

    Arguments:
        elem (obj): a valid ElementTree() object
        strip (bool): a boolean value for striping whitespace (optional)

    Credit: Hay Kranen (https://github.com/hay/xml2json)

    """
    d = {}
    for key, value in elem.attrib.items():
        d['@'+key] = value

    # loop over subelements to merge them
    for subelem in elem:
        v = elem2dict(subelem, strip=strip)
        tag = subelem.tag
        value = v[tag]
        try:
            # add to existing list for this tag
            d[tag].append(value)
        except AttributeError:
            # turn existing entry into a list
            d[tag] = [d[tag], value]
        except KeyError:
            # add a new non-list entry
            d[tag] = value
    text = elem.text
    tail = elem.tail
    if strip:
        # ignore leading and trailing whitespace
        if text:
            text = text.strip()
        if tail:
            tail = tail.strip()

    if tail:
        d['#tail'] = tail

    if d:
        # use #text element if other attributes exist
        if text:
            d["#text"] = text
    else:
        # text is the value if no attributes
        d = text or None

    return {elem.tag: d}


def xml2json(xmlstring, strip=True):
    """
    Convert an XML string into a JSON string.

    Arguments:
        xmlstring (str): a valid XML string
        strip (bool): a boolean value for striping whitespace (optional)

    Credit: Hay Kranen (https://github.com/hay/xml2json)

    """
    elem = ET.fromstring(xmlstring)
    return json.dumps(elem2dict(elem, strip=strip))


# DATATYPE #
# -------- #
class Datatype(object):
    """
    Use the datatype object to create an XML type.

    type (str): a valid XML type
    fields (dict): a dict containing fieldnames and data

    """
    def __init__(self, type, fields):
        self.type = type
        self.fields = fields

    def __str__(self):
        return '"%s" datatype object' % self.type

    def getDatatype(self):
        """
        Return an XML object using the ElementTree library.

        Arguments:
            none

        Returns:
            elem (obj): an <Element> object from the ElementTree library

        """
        elem = ET.Element(self.type)

        if self.type == 'Filter':
            elem.attrib = {'type': 'customer'}
            for key in self.fields:
                subelem = ET.SubElement(elem, key)
                subelem.text = self.fields[key]
        else:
            for key in self.fields:
                address_set = False
                if key in ADDRESS_FIELDS:
                    if not address_set:
                        addr = ET.SubElement(elem, 'addr')
                        address = ET.SubElement(addr, 'Address')
                        address_set = True
                    subelem = ET.SubElement(address, key)
                    subelem.text = self.fields[key]
                elif isinstance(self.fields[key], Datatype):
                    subelem = ET.SubElement(elem, key)
                    subelem.append(self.fields[key].getDatatype())
                else:
                    subelem = ET.SubElement(elem, key)
                    subelem.text = self.fields[key]
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.getDatatype(), 'utf-8')


# QUICKDATE #
# --------- #
class QuickDate(object):
    """
    Use the QuickDate object to create an XML date.

    datestring (str): a valid date string
    format (str): a valid strptime format string

    """
    def __init__(self, datestring, format):
        self.datestring = datestring
        self.format = format

    def __str__(self):
        return '%s (%s)' % (self.datestring, self.format)

    def getQuickDate(self):
        """
        Return an XML object using the ElementTree library.

        Arguments:
            none

        Returns:
            elem (obj): an <Element> object from the ElementTree library

        """
        dt = datetime.strptime(self.datestring, self.format).date()

        elem = ET.Element('Date')
        y = ET.SubElement(elem, 'year')
        y.text = dt.year
        m = ET.SubElement(elem, 'month')
        m.text = dt.month
        d = ET.SubElement(elem, 'day')
        d.text = dt.day

        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.getQuickDate(), 'utf-8')


# COMMANDS #
# -------- #
class Add(object):
    """
    Use the Add command to add records.

    Arguments:
        type (str): a valid XML type
        attrib (dict): a dictionary containing add attributes
        datatype (obj): a valid Datatype() object

    """
    def __init__(self, type, attribs, datatype):
        self.type = type
        self.attribs = attribs
        self.datatype = datatype.getDatatype()

    def __str__(self):
        return 'Add "%s"' % self.type

    # type
    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, t):
        if t == 'User':
            raise Exception(
                'datatype "%s" not supported - use CreateUser' % t
            )
        if t == 'Company':
            raise Exception(
                'datatype "%s" not supported - use CreateAccount' % t
            )
        self._type = t

    def add(self):
        """
        Returns an ElementTree object containing add tags.

        """
        elem = ET.Element('Add')
        attribs = {}
        attribs['type'] = self.type

        # process all add attributes
        for key in self.attribs:
            attribs[key] = self.attribs[key]

        elem.attrib = attribs
        elem.append(self.datatype)
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.add(), 'utf-8')


class Modify(object):
    """
    Use the Modify command to change records.

    Arguments:
        type (str): a valid XML type
        attrib (dict): a dictionary containing modify attributes
        datatype (obj): a valid Datatype() object

    """
    def __init__(self, type, attribs, datatype):
        self.type = type
        self.attribs = attribs
        self.datatype = datatype.getDatatype()

    def __str__(self):
        return 'Modify "%s"' % self.type

    def modify(self):
        """
        Returns an ElementTree object containing modify tags.

        """
        elem = ET.Element('Modify')
        attribs = {}
        attribs['type'] = self.type

        # process all add attributes
        for key in self.attribs:
            attribs[key] = self.attribs[key]

        elem.attrib = attribs
        elem.append(self.datatype)
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.modify(), 'utf-8')


class Switch(object):
    """
    Use the Switch command to set Company and User records.

    Arguments:
        type (str): a valid XML type
        datatype (obj): a valid flag Datatype object

    """
    def __init__(self, type, datatype):
        self.type = type
        self.datatype = datatype

    def __str__(self):
        return '%s flag for "%s"' % (self.type, self.datatype['name'])

    def switch(self):
        """
        Returns an ElementTree object containing a switch tag.

        """
        elem = ET.Element(self.type)
        flags = ET.SubElement(elem, 'flags')
        flags.append(self.datatype.getDatatype())
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.switch(), 'utf-8')


class Error(object):
    """
    Use the Error command to return info about an error code.
    Fetching errors does not require authentication, so this is
    a shortcut without having to create a Datatype and Read
    command separately.

    Arguments:
        code (str): an error code string

    """
    def __init__(self, application, code):
        self.application = application
        self.code = code

    def __str__(self):
        return "Error code %s" % self.code

    def error(self):
        """
        Returns an ElementTree object containing XML error tags.

        """
        request = ET.Element('request')
        request.attrib = {
            'API_ver': '1.0',
            'client': self.application.client,
            'client_ver': self.application.client_version,
            'namespace': self.application.namespace,
            'key': self.application.key
        }

        read = ET.SubElement(request, 'Read')
        read.attrib = {'type': 'Error', 'method': 'equal to'}

        error = ET.SubElement(read, 'Error')
        code = ET.SubElement(error, 'code')
        code.text = self.code
        return request

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        header = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>'
        return header + ET.tostring(self.error(), 'utf-8')


class Report(object):
    """
    Use the Report command to run a report and email a PDF copy
    of a Timesheet, Envelope, or Saved report.

    Arguments:
        type (str): a valid XML type. Only Timesheet, Envelope
                        or Reportf datatypes are allowed
        report (obj): a valid XML report element datatype

    """
    def __init__(self, type, report):
        self.type = type
        self.report = report.getDatatype()

    # type
    @property
    def type(self):
        return self._type

    @type.setter
    def type(self, t):
        if not t in ['Timesheet', 'Envelope', 'Reportf']:
            raise Exception('type "%s" not supported' % t)
        self._type = t

    def getReport(self):
        """
        Returns an ElementTree object containing report tags.

        """
        elem = ET.Element('Report')
        attribs = {}
        attribs['type'] = self.type
        elem.attrib = attribs
        elem.append(self.report)
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.getReport(), 'utf-8')

    def prettify(self):
        """
        Return a formatted, prettified string containing XML tags.

        """
        reparsed = minidom.parseString(self.tostring())
        return reparsed.toprettyxml(indent='  ', encoding='utf-8')


class CreateUser(object):
    """
    Use the CreateUser command to create a new OpenAir user.

    Arguments:
        company (obj): a valid company Datatype() object
        user (obj): a valid user Datatype() object

    """
    def __init__(self, company, user):
        self.company = company
        self.user = user

    def __str__(self):
        return 'CreateUser "%s"' % self.user.fields['nickname']

    # company
    @property
    def company(self):
        return self._company

    @company.setter
    def company(self, c):
        if not 'nickname' in c.fields:
            raise Exception('"nickname" is a required Company field' % c)
        self._company = c

    # user
    @property
    def user(self):
        return self._user

    @user.setter
    def user(self, u):
        if (
            not 'nickname' in u.fields or
            not 'password' in u.fields or
            not 'email' in u.fields
        ):
            raise Exception(
                '"nickname, password and email" are required User fields' % u
            )
        self._user = u

    def create(self):
        """
        Returns an ElementTree object containing submit tags.

        """
        elem = ET.Element('CreateUser')
        elem.append(self.company.getDatatype())
        elem.append(self.user.getDatatype())
        return elem

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.create(), 'utf-8')

    def prettify(self):
        """
        Return a formatted, prettified string containing XML tags.

        """
        reparsed = minidom.parseString(self.tostring())
        return reparsed.toprettyxml(indent='  ', encoding='utf-8')


# CONNECTIONS #
# ----------- #
class Application(object):
    """
    Use the Application command to collect application information.

    Arguments:
        client (str): a client string
        client_version (str): a client_version string
        namespace (str): a namespace string
        key (str): a key string

    """
    def __init__(self, client, client_version, namespace, key):
        self.client = client
        self.client_version = client_version
        self.namespace = namespace
        self.key = key

    def __str__(self):
        return "%s (v%s)" % (self.client, self.version)


class Auth(object):
    """
    Use the Auth command to collect authentication information.

    Arguments:
        company (str): a company string
        username (str): a username string
        password (str): a password string

    """
    def __init__(self, company, username, password):
        self.company = company
        self.username = username
        self.password = password

    def __str__(self):
        return "%s, %s, *****" % (self.company, self.username)

    def auth(self):
        """
        Returns an ElementTree object containing an XML Auth tag.

        """
        auth = ET.Element('Auth')
        login = ET.SubElement(auth, 'Login')

        company = ET.Element('company')
        company.text = self.company

        username = ET.Element('user')
        username.text = self.username

        password = ET.Element('password')
        password.text = self.password

        login.append(company)
        login.append(username)
        login.append(password)
        return auth

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        return ET.tostring(self.auth(), 'utf-8')


class Request(object):
    """
    Use the Request command to create a complete XML request with tags.

    Arguments:
        application (obj): an Application object
        auth (obj): an Auth object
        xml_data (list): a list of Datatype object

    """
    def __init__(self, application, auth, xml_data):
        self.application = application
        self.auth = auth
        self.xml_data = xml_data

    def __str__(self):
        return '"%s" request as %s\%s' % (
            self.application.client,
            self.auth.company,
            self.auth.username
        )

    def request(self):
        """
        Returns an ElementTree object containing an XML request tag
        and associated XML data.

        """
        request = ET.Element('request')
        request.attrib = {
            'API_ver': '1.0',
            'client': self.application.client,
            'client_ver': self.application.client_version,
            'namespace': self.application.namespace,
            'key': self.application.key
        }
        request.append(self.auth.auth())

        if self.xml_data:
            for elem in self.xml_data:
                request.append(elem)

        return request

    def tostring(self):
        """
        Return a string containing XML tags.

        """
        header = '<?xml version="1.0" encoding="utf-8" standalone="yes"?>'
        return header + ET.tostring(self.request(), 'utf-8')
