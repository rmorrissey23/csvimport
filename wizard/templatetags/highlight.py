from django.template.base import (Library, Node, TemplateSyntaxError)

from pygments import highlight as highligher
from pygments.lexers import get_lexer_by_name
from pygments.formatters import HtmlFormatter

register = Library()


class HighlightNode(Node):
    def __init__(self, nodelist, syntax):
        self.nodelist = nodelist
        self.syntax = syntax

    def __repr__(self):
        return "<HighlightNode>"

    def render(self, context):
        code = self.nodelist.render(context)

        try:
            lexer = get_lexer_by_name(self.syntax, stripall=True)
            formatter = HtmlFormatter()
            output = highligher(code, lexer, formatter)
        except (ValueError, TypeError):
            print "Could not determine syntax"
            output = code

        return output


@register.tag
def highlight(parser, token):
    """
    Attempts to highlight code using the pygments library. Falls
    back to no highlighting if syntax isn't

    Usage:
        {% highlight syntax %}
        ...
        {% endhighlight %}

    Example input:
        {% highlight python %}
        print "Hello World"
        {% endhighlight %}

    Example output:
        <div class="highlight">
        <pre><span class="k">print</span> <span class="s">&quot;Hello World&quot;</span></pre>
        </div>

    """
    bits = token.split_contents()

    if len(bits) != 2:
        raise TemplateSyntaxError("'%s' takes one required argument 'syntax'" % bits[0])

    tag, syntax = bits
    nodelist = parser.parse(('endhighlight',))
    parser.delete_first_token()
    return HighlightNode(nodelist, syntax)
