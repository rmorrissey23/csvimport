from __future__ import absolute_import

from django.contrib import messages
# from django.core.exceptions import ValidationError
from django.core.files.storage import default_storage
from django.core.urlresolvers import reverse
from django.forms.formsets import BaseFormSet, formset_factory
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404, render, render_to_response, redirect
from django.template import RequestContext
from django.views.decorators.debug import sensitive_variables

from wizard.forms import ErrorCodeForm, WizardForm

from wizard.models import CSVFile

from credentials import OPENAIR_API
from wizard.api_commands import *
import wizard.oaxmlapi as oaxmlapi

import datetime
from functools import partial, wraps
# from os import path
import re
import urllib2
import xml.dom.minidom as xmlmini

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

try:
    import simplejson as json
except ImportError:
    import json


# HELPER #
# ------ #
def clean_xml_values(string):
    """
    Removes any passwords and secret keys from an XML string.

    """
    safe_xml_string = re.sub(
        r'<password>.*?<\/password>',
        '<password>*****</password>',
        string,
        flags=re.S
    )
    safe_xml_string = re.sub(
        r'\skey="([A-Za-z0-9]*?)"',
        ' key="*****"',
        safe_xml_string,
        count=1
    )
    return safe_xml_string


def get_complex_type_fields(recordtype):
    with open('wizard/openair_wsdl.json') as f:
        json_data = json.load(f)
        fields = json_data[recordtype]
        choices_list = []
        for field in fields:
            choices_list.append(field)
    return json.dumps(choices_list)


# HOME #
# ---- #
def home(request):
    """
    Display the default homepage.
    """
    template = 'index.html'
    args = {'tab': 'home'}

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request)
    )


# HELP #
# ---- #
def faq(request):
    """
    An FAQ page with helpful comments.

    This was previously the "Help" page.
    """
    template = 'faq.html'
    args = {'tab': 'faq'}

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request)
    )


# ADMINISTRATION #
# -------------- #
def administration(request):
    """
    An administration page for managing files.
    """
    all_files = CSVFile.objects.all()
    template = 'administration.html'
    args = {
        'tab': 'administration',
        'all_files': all_files
    }

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request)
    )


# CUSTOM ERRORS #
# ------------- #
def custom403(request):
    """
    A custom 403 error.
    """
    return render(request, '403.html')


def custom404(request):
    """
    A custom 404 error.
    """
    return render(request, '404.html')


def custom500(request):
    """
    A custom 500 error.
    """
    return render(request, '500.html')


# WIZARD #
# ------ #
@sensitive_variables('c_password', 'xml_data')
def wizard(request):
    """
    Display the wizard form.
    """
    if request.POST and request.method == 'POST':
        form = WizardForm(request.POST, request.FILES)

        if form.is_valid():
            # get form fields
            c_recordtype = form.cleaned_data['recordtype']
            c_company = form.cleaned_data['company']
            c_user = form.cleaned_data['user']
            c_password = form.cleaned_data['password']
            c_server = form.cleaned_data['server']
            c_confirm = form.cleaned_data['confirm']

            # process file information
            if form.cleaned_data['csvfile']:
                csvfile = request.FILES['csvfile']

            if c_confirm is True:
                if c_recordtype == 'servertime':
                    xml_data = generate_servertime_data()

                    if xml_data.status == 0:
                        xml_login = create_xml_login(
                            xml_data.data,
                            c_company,
                            c_user,
                            c_password,
                            c_server
                        )

                        if xml_login.status == 0:
                            xml_res = generate_api_request(
                                xml_login.data,
                                c_server
                            )

                            if xml_res.status == 0:
                                messages.info(
                                    request,
                                    xml_res.data,
                                    extra_tags='servertime'
                                )
                            else:
                                messages.error(request, xml_res.data)
                        else:
                            messages.error(request, xml_login.data)
                    else:
                        messages.error(request, xml_data.data)

                elif (c_recordtype == 'customer' or
                      c_recordtype == 'project' or
                      c_recordtype == 'user'):

                    f = process_csv_file(csvfile)

                    if f.status == 0:
                        xml_data = generate_xml_record_data(
                            c_recordtype,
                            f.data['headers'],
                            f.data['reader']
                        )

                        if xml_data.status == 0:
                            xml_login = create_xml_login(
                                xml_data.data,
                                c_company,
                                c_user,
                                c_password,
                                c_server
                            )

                            if xml_login.status == 0:
                                xml_res = generate_api_request(
                                    xml_login.data,
                                    c_server
                                )

                                if xml_res.status == 0:
                                    messages.info(
                                        request,
                                        xml_res.data,
                                        extra_tags='success'
                                    )
                                else:
                                    messages.error(request, xml_res.data)
                            else:
                                messages.error(request, xml_login.data)
                        else:
                            messages.error(request, xml_data.data)
                    else:
                        messages.error(request, f.data)

                else:
                    messages.error(request, 'Invalid recordtype.')

                template = 'log.html'
                args = {'tab': 'wizard'}

                return render_to_response(
                    template,
                    args,
                    context_instance=RequestContext(request)
                )

            else:
                # template = 'index.html'
                template = 'wizard.html'
                args = {
                    'form': form,
                    'tab': 'wizard'
                }

                return render_to_response(
                    template,
                    args,
                    context_instance=RequestContext(request)
                )

    else:
        form = WizardForm()

    template = 'wizard.html'
    args = {'form': form,
            'tab': 'wizard'}

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request))


def wizard_error_code(request):
    """
    Lookup and return an error code for the OpenAir API.
    """
    if request.POST and request.method == 'POST':
        form = ErrorCodeForm(request.POST)

        if form.is_valid():
            error_code = form.cleaned_data['error_code']

            if error_code == 0 or error_code == '0':
                error_message = ('<span class="text-success">'
                                 'The operation was successful</span>.')

            else:
                xml_res = return_error_code(error_code)

                if xml_res.status == 0:
                    error_message = xml_res.data
                else:
                    error_message = 'Unable to find the error code provided'

            template = 'error_code.html'
            args = {'error_code': error_code,
                    'error_message': error_message}

            return render_to_response(
                template,
                args,
                context_instance=RequestContext(request))

        else:
            template = 'error_form.html'
            args = {'form': form}

            return render_to_response(
                template,
                args,
                context_instance=RequestContext(request))

    else:
        form = ErrorCodeForm()

    template = 'error_form.html'
    args = {'form': form}

    return render_to_response(
        template,
        args,
        context_instance=RequestContext(request))


# DELETE FILE #
# ----------- #
@sensitive_variables('uuid')
def delete_file(request, uuid):
    """
    Delete the file from the database.

    """
    if uuid:
        csvfile = get_object_or_404(CSVFile, uuid__exact=uuid)
        filepath = csvfile.file.path

        if default_storage.exists(filepath):
            default_storage.delete(filepath)

        csvfile.delete()

    redirect_url = reverse('administration')
    return HttpResponseRedirect(redirect_url)


@sensitive_variables('redirect_url')
def delete_everything(request):
    """
    Delete all files from the database.

    """
    all_files = CSVFile.objects.all()

    for csvfile in all_files:
        filepath = csvfile.file.path

        if default_storage.exists(filepath):
            default_storage.delete(filepath)

        csvfile.delete()

    redirect_url = reverse('administration')
    return HttpResponseRedirect(redirect_url)