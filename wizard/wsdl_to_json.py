import urllib2

try:
    import xml.etree.cElementTree as ET
except ImportError:
    import xml.etree.ElementTree as ET

try:
    import simplejson as json
except ImportError:
    import json


xs = '{http://www.w3.org/2001/XMLSchema}'
wsdl_url = 'http://www.openair.com/wsdl.pl?wsdl'
req = urllib2.Request(wsdl_url)
res = urllib2.urlopen(req)

tree = ET.parse(res)
root = tree.getroot()

library = {}
for complextype in root.iter(xs + 'complexType'):
    complexname = complextype.get('name')
    if complexname[:2] == 'oa':
        fields = []
        for element in complextype.iter(xs + 'element'):
            field = element.get('name')
            if not field == 'attributes':
                fields.append(field)
        fields.sort()
        library[complexname[2:]] = fields

json_data = json.dumps(library, indent=2, sort_keys=True)

with open('openair_wsdl.json', 'wb') as f:
    json.dump(library, f)
